#!/usr/bin/env python
"""Checks that the correct local installs of necessary python packages are
in use. When called as script relies on stdin to get basepath."""

def check_environment(basepath):
    """Checks that the key imports each are coming from basepath."""
    import os
    assert os.__file__.startswith(basepath)
    #import numpy  
    #assert numpy.__file__.startswith(basepath)
    #import scipy
    #assert scipy.__file__.startswith(basepath)
    import ase
    assert ase.__file__.startswith(basepath)
    import neural
    assert neural.__file__.startswith(basepath)

if __name__ == '__main__':
    import sys
    basepath = sys.argv[-1]

    # Check python environment.
    import os
    print(os.__file__)

    #import numpy
    #print(numpy.__file__)

    #import scipy
    #print(scipy.__file__)

    import ase
    print(ase.__file__)

    import neural
    print(neural.__file__)

    check_environment(basepath)
