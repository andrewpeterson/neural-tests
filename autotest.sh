
originaldirectory=`pwd`

#################################################################
# Create virtual environment (from python script).
#################################################################
testdir="`python helpers/create_environment.py`"
echo $testdir

#################################################################
# Activate virtual environment, zeroing out the pythonpath first.
#################################################################
cd $testdir
export PYTHONPATH=""
source env/bin/activate

#################################################################
# Install standard packages.
#################################################################
#pip install -U setuptools
#pip install -U pip
#pip install numpy
#pip install scipy
pip install -U nose
#pip install ffnet

#################################################################
# Install latest ASE.
#################################################################
pip install svn+https://svn.fysik.dtu.dk/projects/ase/trunk

#################################################################
# Install, compile, and set environment for latest Neural.
#################################################################
mkdir $testdir/neuralcode
cd $testdir/neuralcode
git clone git@bitbucket.org:andrewpeterson/neural.git neural
cd $testdir/neuralcode/neural
f2py -c -m fmodules fmodules.f90
export PYTHONPATH=$testdir/neuralcode:$PYTHONPATH

#################################################################
# Check neural installation.
#################################################################
cd $originaldirectory
python $originaldirectory/helpers/check_python.py $testdir

#################################################################
# Prepare tests folder.
#################################################################
mkdir $testdir/runtests
cd $testdir/runtests
cp -r $testdir/neuralcode/neural/tests $testdir/runtests


#################################################################
# Run tests.
#################################################################
nosetests -v 2>&1 | tee autotest.log

ssh login001 mailx -s \"Neuraltest $testdir.\" $neuraltestmail \<$testdir/runtests/autotest.log
