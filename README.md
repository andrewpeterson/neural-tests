# README #

This repository has scripts to help us automate the running of the tests for the neural package. The script downloads the newest version of both Neural and ASE, then executes the tests from Neural.

It does this by using a cron job to submit the below script as with "sbatch path/to/testneural.sh":

File testneural.sh:
```
#!/bin/bash
#SBATCH --time=10:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --partition=maint-batch
#SBATCH --mail-type=ALL
#SBATCH --mail-user=your_email@yourhost
#
# This script submits the autotests for the neural package.
#
# Neural autotest package:
#  https://bitbucket.org/andrewpeterson/neural-tests
# Neural package:
#  https://bitbucket.org/andrewpeterson/neural

cd neural-tests
./autotest.sh
```